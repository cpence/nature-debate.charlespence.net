.PHONY: lint hint graphs network-movie deploy

lint:
	jslint public/js/app.js public/js/data-manual.js public/js/data.js public/js/edges.js public/js/layout.js public/js/nodes.js
hint:
	jshint public/js/app.js public/js/data-manual.js public/js/data.js public/js/edges.js public/js/layout.js public/js/nodes.js

network-movie:
	rm -rf frames
	mkdir frames
	phantomjs scripts/phantomjs-movie.js
	ffmpeg -y -c:v png -r 60 -i frames/frame%d.png -c:v libx264 -crf 18 -preset slower -pix_fmt yuv420p -movflags faststart network-movie.mp4
	rm -rf frames

screenshots:
	phantomjs scripts/phantomjs-screenshots.js

graphs:
	cd data && gnuplot '011 - Publication Year Graph.gnu'

deploy:
	cat node_modules/es5-shim/es5-shim.js node_modules/es5-shim/es5-sham.js node_modules/es6-shim/es6-shim.js node_modules/es6-shim/es6-sham.js node_modules/jquery/dist/jquery.js node_modules/what-input/dist/what-input.js node_modules/foundation-sites/dist/js/foundation.js node_modules/sigma/build/sigma.min.js js/sigma.canvas.nodes.outlined.js node_modules/sigma/build/plugins/sigma.layout.forceAtlas2.min.js node_modules/sigma/build/plugins/sigma.plugins.animate.min.js js/sigma.layout.noverlap.js node_modules/chroma-js/chroma.js js/data.js js/data-manual.js js/nodes.js js/edges.js js/layout.js js/app.js > public/js/bundle.js
	yarn run uglifyjs -o public/js/bundle.min.js public/js/bundle.js
	rm public/js/bundle.js
	rsync -acvz --exclude=/.well-known --exclude=/.htaccess --delete public/ reclaimhosting.com:~/nature-debate

