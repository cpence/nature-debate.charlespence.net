#!/usr/bin/env ruby
require 'csv'
require 'json'
require_relative './frequency-csv-to-weights'

infiles = [
  '022 - Author Name WF Pre 1885.csv',
  '022 - Author Name WF 1885-1889.csv',
  '022 - Author Name WF 1890-1894.csv',
  '022 - Author Name WF 1895-1899.csv',
  '022 - Author Name WF 1900-1904.csv',
  '022 - Author Name WF 1905-1909.csv',
  '022 - Author Name WF 1910-1914.csv',
  '022 - Author Name WF Post 1914.csv'
]

timestamps = [1880, 1885, 1890, 1895, 1900, 1905, 1910, 1915]

all_data = {}

infiles.each_with_index do |infile, i|
  in_data = CSV.read(infile)
  out_data = frequency_csv_to_weights(in_data)

  out_data.each do |edge, weight|
    all_data[edge] ||= []
    all_data[edge] << [timestamps[i], weight]
  end
end

# Fill zeros for missing timestamps
all_data.each do |edge, weights|
  timestamps_present = weights.map { |w| w[0] }
  timestamps_missing = timestamps - timestamps_present
  timestamps_missing.each do |t|
    all_data[edge] << [t, 0]
  end
end

all_nodes = all_data.keys.flatten.uniq
num_nodes = all_nodes.size

# Compute node size tables
node_weights = [{}, {}, {}, {}, {}, {}, {}, {}]
max_node_size = 0
max_edge_weight = 0

all_nodes.each do |node|
  all_data.each do |edge, weights|
    next unless edge.include?(node)
    weights.each do |(timestamp, weight)|
      idx = timestamps.index(timestamp)
      node_weights[idx][node] ||= 0
      node_weights[idx][node] += weight

      max_edge_weight = weight if weight > max_edge_weight
    end
  end
end

max_node_weight = node_weights.map { |h| h.values }.flatten.max.to_f

outfile = File.open('023 - Author Graph Inputs.js', 'wb')
outfile << "/*jslint indent: 2 */\n"
outfile << "\"use strict\";\n\n"

outfile << "var VisualizationData = (function () {\n"
outfile << "  var mod = {};\n\n"

# Output node names
outfile << "  // -------------------- NODE NAMES --------------------\n"
outfile << "  mod.NodeNames = [\n"
outfile << all_nodes.map { |n| "    '#{n}'" }.join(",\n").concat("\n")
outfile << "  ];\n\n\n"

# Output node size tables
outfile << "  // -------------------- NODE SIZE TABLES --------------------\n"
outfile << "  mod.NodeSizes = new Array(8);\n"

node_weights.each_with_index do |hash, i|
  outfile << "  mod.NodeSizes[#{i}] = {\n"
  outfile << hash.map { |k, v| "    #{k}: #{v.to_f / max_node_weight}" }.join(",\n").concat("\n")
  outfile << "  };\n"
end

outfile << "\n\n"

# Output edge weight tables
outfile << "  // -------------------- EDGE WEIGHT TABLES --------------------\n"
outfile << "  mod.MaxEdgeWeight = #{max_edge_weight};\n"
outfile << "  mod.EdgeWeights = new Array(8);\n"

timestamps.each_with_index do |ts, i|
  outfile << "  mod.EdgeWeights[#{i}] = {\n"

  ts_weights = {}
  all_data.each do |edge, weights|
    weight = weights.find { |w| w[0] == ts }.last
    ts_weights[edge[0]] ||= {}
    ts_weights[edge[0]][edge[1]] = weight
  end

  ts_weight_strings = {}
  ts_weights.each do |k, v|
    ts_weight_strings[k] = v.map { |hk, hv| "'#{hk}': #{hv}" }.join(', ')
  end
  outfile << ts_weights.map { |k, v| "    #{k}: {#{ts_weight_strings[k]}}" }.join(",\n").concat("\n")

  outfile << "  };\n"
end

outfile << "\n\n"

outfile << "  return mod;\n"
outfile << "}());\n\n"

outfile.close
