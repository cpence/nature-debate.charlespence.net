#!/usr/bin/env ruby
require 'csv'

# This script runs through the CSV file given and carves it up by year.

infile = '021 - Author Name Word Frequency Year Final.csv'

out_pre = '022 - Author Name WF Pre 1885.csv'
out_85 = '022 - Author Name WF 1885-1889.csv'
out_90 = '022 - Author Name WF 1890-1894.csv'
out_95 = '022 - Author Name WF 1895-1899.csv'
out_00 = '022 - Author Name WF 1900-1904.csv'
out_05 = '022 - Author Name WF 1905-1909.csv'
out_10 = '022 - Author Name WF 1910-1914.csv'
out_post = '022 - Author Name WF Post 1914.csv'

csv_pre = CSV.open(out_pre, "wb")
csv_85 = CSV.open(out_85, "wb")
csv_90 = CSV.open(out_90, "wb")
csv_95 = CSV.open(out_95, "wb")
csv_00 = CSV.open(out_00, "wb")
csv_05 = CSV.open(out_05, "wb")
csv_10 = CSV.open(out_10, "wb")
csv_post = CSV.open(out_post, "wb")

in_data = CSV.read(infile)
header_row = in_data[3]

csv_pre << in_data[0]
csv_85 << in_data[0]
csv_90 << in_data[0]
csv_95 << in_data[0]
csv_00 << in_data[0]
csv_05 << in_data[0]
csv_10 << in_data[0]
csv_post << in_data[0]

csv_pre << in_data[1]
csv_85 << in_data[1]
csv_90 << in_data[1]
csv_95 << in_data[1]
csv_00 << in_data[1]
csv_05 << in_data[1]
csv_10 << in_data[1]
csv_post << in_data[1]

csv_pre << in_data[2]
csv_85 << in_data[2]
csv_90 << in_data[2]
csv_95 << in_data[2]
csv_00 << in_data[2]
csv_05 << in_data[2]
csv_10 << in_data[2]
csv_post << in_data[2]

header_pre = ['']
header_85 = ['']
header_90 = ['']
header_95 = ['']
header_00 = ['']
header_05 = ['']
header_10 = ['']
header_post = ['']

next_pre = ['']
next_85 = ['']
next_90 = ['']
next_95 = ['']
next_00 = ['']
next_05 = ['']
next_10 = ['']
next_post = ['']

header_row[1..-1].each do |column_id|
  parts = column_id.split(' | ')

  year = parts.shift.to_i
  authors = parts.join(' | ')

  if year < 1885
    header_pre << authors
    next_pre << year
  elsif year < 1890
    header_85 << authors
    next_85 << year
  elsif year < 1895
    header_90 << authors
    next_90 << year
  elsif year < 1900
    header_95 << authors
    next_95 << year
  elsif year < 1905
    header_00 << authors
    next_00 << year
  elsif year < 1910
    header_05 << authors
    next_05 << year
  elsif year < 1915
    header_10 << authors
    next_10 << year
  else
    header_post << authors
    next_post << year
  end
end

csv_pre << header_pre
csv_85 << header_85
csv_90 << header_90
csv_95 << header_95
csv_00 << header_00
csv_05 << header_05
csv_10 << header_10
csv_post << header_post

csv_pre << next_pre
csv_85 << next_85
csv_90 << next_90
csv_95 << next_95
csv_00 << next_00
csv_05 << next_05
csv_10 << next_10
csv_post << next_post

in_data.drop(5).each do |row|
  row_id = row[0]

  pre_row = [row_id]
  row85 = [row_id]
  row90 = [row_id]
  row95 = [row_id]
  row00 = [row_id]
  row05 = [row_id]
  row10 = [row_id]
  post_row = [row_id]

  1.upto(row.size - 1).each do |i|
    column_id = header_row[i]
    parts = column_id.split(' | ')

    year = parts.shift.to_i
    authors = parts.join(' | ')

    if year < 1885
      pre_row << row[i]
    elsif year < 1890
      row85 << row[i]
    elsif year < 1895
      row90 << row[i]
    elsif year < 1900
      row95 << row[i]
    elsif year < 1905
      row00 << row[i]
    elsif year < 1910
      row05 << row[i]
    elsif year < 1915
      row10 << row[i]
    else
      post_row << row[i]
    end
  end

  csv_pre << pre_row
  csv_85 << row85
  csv_90 << row90
  csv_95 << row95
  csv_00 << row00
  csv_05 << row05
  csv_10 << row10
  csv_post << post_row
end

csv_pre.close
csv_85.close
csv_90.close
csv_95.close
csv_00.close
csv_05.close
csv_10.close
csv_post.close
