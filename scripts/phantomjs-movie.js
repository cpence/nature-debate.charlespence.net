var page = require('webpage').create();
page.viewportSize = {
  width: 1024,
  height: 800
};
page.clipRect = {
  top: 0,
  left: 0,
  width: 1024,
  height: 800
};

// Thanks to https://www.sitepoint.com/creating-accurate-timers-in-javascript/
function doTimer(length, resolution, oninstance, oncomplete) {
  var steps = (length / 1000.0) * resolution,
    speed = length / steps,
    count = 0,
    start = new Date().getTime();

  function instance() {
    var diff;

    if (count++ == steps) {
      oncomplete(steps, count);
    } else {
      oninstance(steps, count);

      diff = (new Date().getTime() - start) - (count * speed);
      window.setTimeout(instance, (speed - diff));
    }
  }

  window.setTimeout(instance, speed);
}

function go() {
  // Click the animate button
  page.evaluate(function () {
    $('#play-button').click();
  });

  // Render at the requested FPS
  var frame = 0;

  doTimer(200000, 5, function() {
    page.render('frames/frame' + (frame++) + '.png', { format: 'png' });
  },
  function() {
    phantom.exit();
  })
}

page.open('http://localhost:8000/', function () {
  setTimeout(go, 30000);
});
