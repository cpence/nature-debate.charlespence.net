
def fix_word(word)
  if word == 'naegeli' || word == 'nägeli'
    return 'nageli'
  end
  if word == 'koelliker' || word == 'kölliker'
    return 'kolliker'
  end
  return word
end

def frequency_csv_to_weights(in_data)
  out_data = {}

  header_row = in_data[3]
  in_data.drop(5).each do |row|
    word_one = ''

    row.each_with_index do |str, i|
      if i == 0
        word_one = fix_word(str)
        next
      end

      num = str.to_i
      next if num == 0

      words_twos = header_row[i].split(' | ')
      words_twos.each do |word_two|
        word_two = fix_word(word_two)
        next if word_one == word_two

        words = [word_one, word_two].sort
        out_data[words] ||= 0
        out_data[words] += num
      end
    end
  end

  out_data
end

def frequency_csv_to_weights_timestamp(in_data)
  out_data = {}

  header_row = in_data[3]
  in_data.drop(5).each do |row|
    word_one = ''

    row.each_with_index do |str, i|
      if i == 0
        word_one = str
        next
      end

      num = str.to_i
      next if num == 0

      words_twos = header_row[i].split(' | ')
      words_twos.each do |word_two|
        next if word_one == word_two

        words = [word_one, word_two].sort
        out_data[words] ||= 0
        out_data[words] += num
      end
    end
  end

  out_data
end

