#!/usr/bin/env ruby
require 'csv'
require_relative './frequency-csv-to-weights'

# This script runs through the CSV file and converts the columns of frequency
# data to graph nodes.

infile = '016 - Author Name Word Frequency Final.csv'
out_weight = '017 - Author Graph With Weight.csv'

in_data = CSV.read(infile)
out_data = frequency_csv_to_weights(in_data)

csv_weight = CSV.open(out_weight, "wb")

out_data.each do |edge, weight|
  weight.times do
    csv_weight << edge
  end
end
