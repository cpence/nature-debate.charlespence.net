#!/usr/bin/env ruby
require 'csv'
require 'cgi'
require 'net/http'
require 'json'

# This script runs through the header row of the given CSV file and converts
# the internal IDs of the articles into their lists of authors *and* the year
# of publication.

infile = '014 - Author Name Word Frequency Clean.csv'
outfile = '020 - Author Name Word Frequency AuthorYear.csv'

CSV.open(outfile, "wb") do |out|
  CSV.foreach(infile).each_with_index do |row, index|
    if index != 3
      out << row
      next
    end

    out << row.map do |str|
      m = %r{Block #1/1 \(within '(.*)'\)}.match(str)

      if m
        uid = m[1]
        uri = URI("https://www.evotext.org/search?q=uid:#{CGI.escape(uid)}")

        args = { q: "uid:\"#{uid}\"", rows: "1", advanced: "true" }
        uri.query = URI.encode_www_form(args)

        http = Net::HTTP.new(uri.host, uri.port)
        http.use_ssl = true

        req = Net::HTTP::Get.new(uri.request_uri)
        req['Accept'] = 'application/json'

        res = http.request(req)

        unless res.is_a?(Net::HTTPSuccess)
          fail "HTTP failure in querying evoText"
        end

        hash = JSON.parse(res.body)
        unless hash['results'] && hash['results']['documents'] &&
               hash['results']['documents'].is_a?(Array)
          fail "Malformed response in querying evoText"
        end

        doc = hash['results']['documents'][0]
        ret = doc['authors'].map { |a| a['full'] }.join(' | ')

        "#{doc['year']} | #{ret}"
      else
        str
      end
    end
  end
end
