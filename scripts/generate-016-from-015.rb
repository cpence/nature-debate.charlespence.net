#!/usr/bin/env ruby
require 'csv'

# This script runs through the header row of the given CSV file and converts
# the lists of authors (from word-frequency-to-authors) to the match words.

WORDS = %w{bateson weldon pearson punnett biffen hurst davenport lock darbishire schuster yule jennings pearl shull castle wilson drinkwater nettleship gossage johannsen farabee nillson-ehle emerson nageli naegeli nägeli thiselton lankester romanes driesch eimer morgan cunningham kolliker koelliker kölliker poulton huxley weismann wilson henslow przibram tschermak cockerell vries kriby boulenger forbes goldschmidt correns doncaster brooks willis gregory studnicka guppy bernard vavilov lotsy ewart watson kammerer spencer harmer loeb harrison hagedoorn thompson gray distant slye fisher frankland snyder williams osburn minchin leahy thomson sclater stelfox roth belling pocock jordan beal williston cope boycott thomas guyer rheinhardt curtis orton crew bruce pantin lloyd swinhoe kirby}

infile = '015 - Author Name Word Frequency Biblio.csv'
outfile = '016 - Author Name Word Frequency Final.csv'

CSV.open(outfile, "wb") do |out|
  CSV.foreach(infile).each_with_index do |row, index|
    if index != 3
      out << row
      next
    end

    ret = []

    row.each do |str|
      unless str && !str.empty?
        ret << ''
        next
      end

      words = []

      authors = str.gsub('-', ' ').split(' | ')
      authors.each do |a|
        a.downcase!

        WORDS.each do |w|
          if a.include?(" #{w}")
            words << w
            next
          end
        end
      end

      if words.empty?
        puts "WARNING: Could not find a match for #{str}!"
      end

      ret << words.join(' | ')
    end

    out << ret
  end
end

