var page = require('webpage').create();
page.viewportSize = {
  width: 1920,
  height: 1080
};
page.clipRect = {
  top: 0,
  left: 0,
  width: 1920,
  height: 1080
};

function go() {
  // Click each of the major buttons, then take a shot
  var i, buttons;

  buttons = ['one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight'];

  buttons.forEach(function (button) {
    page.evaluate(function (b) {
      console.log('clicking ' + '#' + b + '-button');
      $('#' + b + '-button').click();
    }, button);

    page.render('screenshot-' + button + '.png', { format: 'png' });
  });

  phantom.exit();
}

page.open('http://localhost:8000/', function () {
  setTimeout(go, 30000);
});
