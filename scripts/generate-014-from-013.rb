#!/usr/bin/env ruby
require 'csv'

# This script keeps only the frequency columns from a very large frequency
# analysis CSV file.

infile = '013 - Author Name Word Frequency Raw.csv'
outfile = '014 - Author Name Word Frequency Clean.csv'

CSV.open(outfile, "wb") do |out|
  CSV.foreach(infile) do |row|
    out << row.select.each_with_index do |_, index|
      (index == 0) || ((index - 1) % 4 == 0)
    end
  end
end
