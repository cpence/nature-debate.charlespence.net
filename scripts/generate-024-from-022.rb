#!/usr/bin/env ruby
require 'csv'
require_relative './frequency-csv-to-weights'

infiles = [
  '022 - Author Name WF Pre 1885.csv',
  '022 - Author Name WF 1885-1889.csv',
  '022 - Author Name WF 1890-1894.csv',
  '022 - Author Name WF 1895-1899.csv',
  '022 - Author Name WF 1900-1904.csv',
  '022 - Author Name WF 1905-1909.csv',
  '022 - Author Name WF 1910-1914.csv',
  '022 - Author Name WF Post 1914.csv'
]

outfiles = [
  '024 - Author Graph Pre 1885.csv',
  '024 - Author Graph 1885-1889.csv',
  '024 - Author Graph 1890-1894.csv',
  '024 - Author Graph 1895-1899.csv',
  '024 - Author Graph 1900-1904.csv',
  '024 - Author Graph 1905-1909.csv',
  '024 - Author Graph 1910-1914.csv',
  '024 - Author Graph Post 1914.csv'
]

infiles.each_with_index do |infile, i|
  in_data = CSV.read(infile)
  out_data = frequency_csv_to_weights(in_data)

  csv_weight = CSV.open(outfiles[i], "wb")

  out_data.each do |edge, weight|
    weight.times do
      csv_weight << edge
    end
  end
end

