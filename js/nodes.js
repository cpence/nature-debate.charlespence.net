/*jslint indent: 2, browser: true */
/*global VisualizationData, chroma */
"use strict";

function defaultNodeFor(id) {
  return {
    type: 'outlined',
    id: id,
    label: VisualizationData.NodeLabels[id],
    x: Math.random() * 5,
    y: Math.random() * 5,
    size: 0.5,
    color: VisualizationData.Palette[0],
    originalColor: VisualizationData.Palette[0]
  };
}

function lerpColor(lerp, one, two) {
  if (one === undefined && two === undefined) {
    return VisualizationData.Palette[0];
  }

  if (one === undefined) {
    one = two;
  } else if (two === undefined) {
    two = one;
  }

  var s = chroma.scale([one, two]).mode('lab');
  return s(lerp).hex();
}

function updateNodeSize(index, rem) {
  // First, update visibility
  VisualizationData.NodeNames.forEach(function (id) {
    var size, node, attrs;

    if (rem === 0.0) {
      size = VisualizationData.NodeSizes[index][id];
    } else {
      size = VisualizationData.NodeSizes[index][id] * (1.0 - rem) + VisualizationData.NodeSizes[index + 1][id] * rem;
    }
    size = VisualizationData.ScaleFactor * Math.pow(size, VisualizationData.NodePower);

    if (size === 0.0) {
      // Delete the node if it's currently on the graph
      node = window.sig.graph.nodes().find(function (n) {
        return n.id === id;
      });

      if (node !== undefined) {
        window.sig.graph.dropNode(id);
      }
    } else {
      node = window.sig.graph.nodes().find(function (n) {
        return n.id === id;
      });

      if (node === undefined) {
        // Create the node if it's not currently on the graph
        attrs = defaultNodeFor(id);
        attrs.size = size;
        window.sig.graph.addNode(attrs);
      } else {
        // Update the node's size
        node.size = size;
      }
    }
  });
}

function updateNodeColor(index, rem) {
  window.sig.graph.nodes().forEach(function (n) {
    var color;

    if (rem === 0.0) {
      color = VisualizationData.NodeColors[index][n.id];
    } else {
      color = lerpColor(rem, VisualizationData.NodeColors[index][n.id], VisualizationData.NodeColors[index + 1][n.id]);
    }

    if (n.color === '#eee') {
      n.originalColor = color;
    } else {
      n.color = color;
      n.originalColor = color;
    }
  });
}

function updateNodePosition(index, rem) {
  // If there isn't any layout data yet, don't try
  if (VisualizationData.BuildingLayouts !== false) {
    return;
  }

  window.sig.graph.nodes().forEach(function (n) {
    var old_pos, new_pos;

    old_pos = VisualizationData.Layouts[index][n.id];
    if (rem === 0.0) {
      n.x = old_pos.x;
      n.y = old_pos.y;
    } else {
      new_pos = VisualizationData.Layouts[index + 1][n.id];

      if (old_pos === undefined) {
        old_pos = new_pos;
      } else if (new_pos === undefined) {
        new_pos = old_pos;
      }

      n.x = old_pos.x * (1.0 - rem) + new_pos.x * rem;
      n.y = old_pos.y * (1.0 - rem) + new_pos.y * rem;
    }
  });
}
