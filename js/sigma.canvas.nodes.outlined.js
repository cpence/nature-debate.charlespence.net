'use strict';

sigma.utils.pkg('sigma.canvas.nodes');
sigma.canvas.nodes.outlined = function(node, context, settings) {
  var prefix = settings('prefix') || '';

  context.fillStyle = node.color || settings('defaultNodeColor');
  context.strokeStyle = '#333';
  context.lineWidth = 1;

  context.beginPath();
  context.arc(
    node[prefix + 'x'],
    node[prefix + 'y'],
    node[prefix + 'size'],
    0,
    Math.PI * 2,
    true
  );

  context.closePath();
  context.fill();
  context.stroke();
};
