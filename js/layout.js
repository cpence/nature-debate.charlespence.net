/*jslint indent: 2, browser: true */
/*global $, updateNodeSize, updateEdges, updateAll */
"use strict";

var VisualizationData = (function (mod) {
  mod.ForceAtlasParameters = {
    scalingRatio: 5.0,
    edgeWeightInfluence: 1.0,
    startingIterations: 20000
  };

  mod.Layouts = new Array(8);
  mod.Layouts[0] = {};
  mod.Layouts[1] = {};
  mod.Layouts[2] = {};
  mod.Layouts[3] = {};
  mod.Layouts[4] = {};
  mod.Layouts[5] = {};
  mod.Layouts[6] = {};
  mod.Layouts[7] = {};

  mod.CurrentBuildingLayout = -1;

  mod.BuildingLayouts = true;

  mod.CameraX = 0;
  mod.CameraY = 0;
  mod.CameraRatio = 1;

  return mod;
}(this.VisualizationData));

function resetCamera() {
  window.sig.camera.x = VisualizationData.CameraX;
  window.sig.camera.y = VisualizationData.CameraY;
  window.sig.camera.ratio = VisualizationData.CameraRatio;

  window.sig.refresh();
}

function finishLayouts() {
  var i, j,
    n,
    x_sum = 0,
    y_sum = 0,
    node_count = 0,
    center_x,
    center_y,
    max_x = -999,
    min_x = 999,
    max_y = -999,
    min_y = 999,
    x_size,
    y_size;

  // We're done building layouts
  VisualizationData.BuildingLayouts = false;

  // Figure out what the best camera position is across the timestamps
  for (i = 0; i < 8; i += 1) {
    updateAll(i, 0);
    window.sig.refresh();

    for (j = 0; j < window.sig.graph.nodes().length; j += 1) {
      n = window.sig.graph.nodes()[j];

      node_count += 1.0;

      x_sum += n.x;
      y_sum += n.y;

      if (n.x > max_x) {
        max_x = n.x;
      } else if (n.x < min_x) {
        min_x = n.x;
      }

      if (n.y > max_y) {
        max_y = n.y;
      } else if (n.y < min_y) {
        min_y = n.y;
      }
    }
  }

  // Compute the camera values and save them out
  center_x = x_sum / node_count;
  center_y = y_sum / node_count;

  VisualizationData.CameraX = center_x;
  VisualizationData.CameraY = center_y;

  x_size = (max_x - min_x) / $('#sigma-container').width();
  y_size = (max_y - min_y) / $('#sigma-container').height();

  if (x_size > y_size) {
    VisualizationData.CameraRatio = x_size;
  } else {
    VisualizationData.CameraRatio = y_size;
  }

  resetCamera();

  // Reset the display to timestamp zero
  updateAll(0, 0);

  // Turn off the loading overlay
  $('#loading-overlay').css('display', 'none');
}

function layoutWorkFinished() {
  var l, fa;

  // If we're finishing up a run of FA2, kill FA2 and run noverlap
  if (window.sig.isForceAtlas2Running()) {
    window.sig.killForceAtlas2();
    window.sig.startNoverlap();
  }

  if (VisualizationData.CurrentBuildingLayout !== -1) {
    // Save off the data
    l = VisualizationData.Layouts[VisualizationData.CurrentBuildingLayout];
    window.sig.graph.nodes().forEach(function (n) {
      l[n.id] = { x: n.x, y: n.y };
    });
  }

  if (VisualizationData.CurrentBuildingLayout === 7) {
    // Just built the last one, we're finished
    finishLayouts();
    return;
  }

  // Bump the layout and reposition all nodes and edges
  VisualizationData.CurrentBuildingLayout += 1;
  updateNodeSize(VisualizationData.CurrentBuildingLayout, 0);
  updateEdges(VisualizationData.CurrentBuildingLayout, 0);
  window.sig.refresh();

  // Run the layout
  fa = window.sig.startForceAtlas2(VisualizationData.ForceAtlasParameters);
  // Watch for it to finish
  fa.supervisor.worker.addEventListener(fa.supervisor.msgName,
                                        layoutWorkFinished);
}

function buildLayouts() {
  // Turn on the loading overlay
  $('#loading-overlay').css('display', 'block');

  // Set some configuration for noverlap
  window.sig.configNoverlap({
    scaleNodes: 1.5,
    easing: undefined
  });

  // Start up the first layout and watcher
  layoutWorkFinished();
}
