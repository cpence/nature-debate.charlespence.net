/*jslint indent: 2, browser: true */
/*global VisualizationData, chroma */
"use strict";

function getEdgeFor(index, rem, source, target) {
  var size_one = 0, size_two = 0, size, weights, weight, node, color;

  weights = VisualizationData.EdgeWeights[index][source];
  if (weights !== undefined) {
    weight = weights[target];
    if (weight !== undefined) {
      size_one = weight;
    }
  }

  if (rem === 0.0) {
    size = size_one;
  } else {
    weights = VisualizationData.EdgeWeights[index + 1][source];
    if (weights !== undefined) {
      weight = weights[target];
      if (weight !== undefined) {
        size_two = weight;
      }
    }

    size = size_one * (1.0 - rem) + size_two * rem;
  }
  size /= VisualizationData.MaxEdgeWeight;

  node = window.sig.graph.nodes().find(function (n) {
    return n.id === source;
  });
  if (node === undefined) {
    color = VisualizationData.Palette[0];
  } else {
    color = node.color;
  }

  return {
    id: source + '-' + target,
    source: source,
    target: target,
    size: VisualizationData.ScaleFactor * Math.pow(size, VisualizationData.EdgePower),
    weight: size,
    color: color
  };
}

function updateEdges(index, rem) {
  VisualizationData.NodeNames.forEach(function (source) {
    VisualizationData.NodeNames.forEach(function (target) {
      var attrs, edge;

      // We include edge data for edges where source < target
      if (source < target) {
        attrs = getEdgeFor(index, rem, source, target);

        if (attrs === undefined || attrs.size < 0.0001) {
          // Delete this edge if it's not on the graph
          edge = window.sig.graph.edges().find(function (e) {
            return e.source === source && e.target === target;
          });

          if (edge !== undefined) {
            window.sig.graph.dropEdge(edge.id);
          }
        } else {
          edge = window.sig.graph.edges().find(function (e) {
            return e.source === source && e.target === target;
          });

          if (edge === undefined) {
            // Add this edge if it's not in the graph
            window.sig.graph.addEdge(attrs);
          } else {
            // Update its size, weight, and colors
            edge.size = attrs.size;
            edge.weight = attrs.weight;

            if (edge.color === '#eee') {
              edge.originalColor = attrs.color;
            } else {
              edge.color = attrs.color;
              edge.originalColor = attrs.color;
            }
          }
        }
      }
    });
  });
}
