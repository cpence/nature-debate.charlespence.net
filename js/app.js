/*jslint indent: 2, browser: true */
/*global $, VisualizationData, chroma, sigma, updateNodeSize, defaultNodeFor, updateNodeColor, updateNodePosition, updateEdges, resetCamera, buildLayouts */
"use strict";

var Sigma = sigma;

var VisualizationData = (function (mod) {
  mod.ScaleFactor = 20.0;
  mod.NodePower = 0.3;
  mod.EdgePower = 0.7;

  return mod;
}(this.VisualizationData));

function updateAll(index, rem) {
  updateNodeSize(index, rem);
  updateNodeColor(index, rem);
  updateNodePosition(index, rem);
  updateEdges(index, rem);

  window.sig.refresh();
}

function doAnimation(year) {
  var index_float, index, rem;

  index_float = (year - 1880.0) / 8.0;
  if (Math.abs(index_float - window.lastFloat) < 0.01) {
    return;
  }

  // Make sure we're in bounds
  if (index_float < 0.0 || index_float > 7.0) {
    return;
  }

  index = Math.floor(index_float);
  rem = index_float - index;

  updateAll(index, rem);
}

function animateCallback(now) {
  doAnimation(now / 1000.0);
}

function forceSliderTo(year) {
  var sliderVal, handleDim, elemDim, pxToMove, move;

  handleDim = $('.slider-handle')[0].getBoundingClientRect().height;
  elemDim = $('#date-slider')[0].getBoundingClientRect().height;

  sliderVal = 35 - (year - 1880);
  pxToMove = (elemDim - handleDim) * (sliderVal / 35.0);
  move = ((pxToMove / elemDim) * 100).toFixed(2);

  $('.slider-handle').css('top', move + '%');
}

function animateAndSliderCallback(now) {
  animateCallback(now);
  forceSliderTo(now / 1000.0);
}

function animateGraph(start, end, msec, stop, moveSlider) {
  // Don't let this interfere with our layout building process, if a trigger
  // fires before we're done for some reason
  if (VisualizationData.BuildingLayouts !== false) {
    return;
  }

  if (start !== undefined) {
    // undefined means take where we are currently, otherwise set it
    $('#sigma-container').css('background-position-x', start * 1000.0);
  }

  if (stop) {
    // stop means kill any animation currently running
    $('#simga-container').stop(true, true);
  }

  $('#sigma-container').animate({
    'background-position-x': String(end * 1000)
  }, {
    duration: msec,
    step: moveSlider ? animateAndSliderCallback : animateCallback
  })
}

$('#date-slider').on('moved.zf.slider', function () {
  var year = 35 - Number($(this).children('.slider-handle').attr('aria-valuenow'));
  year += 1880;

  if (year !== window.currentYear) {
    window.lastYear = window.currentYear;
    window.currentYear = year;

    animateGraph(undefined, year, 200);
  }
});

$('#play-button').on('click', function () {
  var msec;

  // Make the animation exceptionally slow under PhantomJS, as we're going
  // to be rendering a movie
  msec = (window._phantom) ? 200000 : 20000;

  animateGraph(1880, 1915, msec, true, true);
});

$('#one-button').on('click', function () {
  updateAll(0, 0);
  forceSliderTo(1880);
});
$('#two-button').on('click', function () {
  updateAll(1, 0);
  forceSliderTo(1885);
});
$('#three-button').on('click', function () {
  updateAll(2, 0);
  forceSliderTo(1890);
});
$('#four-button').on('click', function () {
  updateAll(3, 0);
  forceSliderTo(1895);
});
$('#five-button').on('click', function () {
  updateAll(4, 0);
  forceSliderTo(1900);
});
$('#six-button').on('click', function () {
  updateAll(5, 0);
  forceSliderTo(1905);
});
$('#seven-button').on('click', function () {
  updateAll(6, 0);
  forceSliderTo(1910);
});
$('#eight-button').on('click', function () {
  updateAll(7, 0);
  forceSliderTo(1915);
});

$('#one-anim-button').on('click', function () {
  animateGraph(1880, 1885, 5000, true, true);
});
$('#two-anim-button').on('click', function () {
  animateGraph(1885, 1890, 5000, true, true);
});
$('#three-anim-button').on('click', function () {
  animateGraph(1890, 1895, 5000, true, true);
});
$('#four-anim-button').on('click', function () {
  animateGraph(1895, 1900, 5000, true, true);
});
$('#five-anim-button').on('click', function () {
  animateGraph(1900, 1905, 5000, true, true);
});
$('#six-anim-button').on('click', function () {
  animateGraph(1905, 1910, 5000, true, true);
});
$('#seven-anim-button').on('click', function () {
  animateGraph(1910, 1915, 5000, true, true);
});


$('#camera-button').on('click', function () {
  resetCamera();
});

function initializeGraph() {
  var g = {
    nodes: [],
    edges: []
  };

  window.currentYear = 1880;
  window.lastYear = 1880;
  window.lastFloat = 1880.0;
  $('#sigma-container').css('background-position-x', 1880000);

  Sigma.classes.graph.addMethod('neighbors', function (nodeId) {
    var k, neighbors = {}, index = this.allNeighborsIndex[nodeId] || {};

    for (k in index)
      neighbors[k] = this.nodesIndex[k];

    return neighbors;
  });

  window.sig = new Sigma({
    graph: g,
    renderer: {
      container: 'sigma-container',
      type: 'canvas'
    },
    settings: {
      labelThreshold: 999999,

      minEdgeSize: 0.0001,
      maxEdgeSize: 10,
      minNodeSize: 1,
      maxNodeSize: 15,

      autoRescale: false,
      zoomMax: 5.0
    }
  });

  buildLayouts();

  window.sig.bind('clickNode', function (e) {
    var nodeId = e.data.node.id, toKeep = window.sig.graph.neighbors(nodeId);
    toKeep[nodeId] = e.data.node;

    window.sig.graph.nodes().forEach(function (n) {
      if (toKeep[n.id]) {
        n.color = n.originalColor;
      } else {
        n.color = '#eee';
      }
    });

    window.sig.graph.edges().forEach(function (e) {
      if (toKeep[e.source] && toKeep[e.target]) {
        e.color = e.originalColor;
      } else {
        e.color = '#eee';
      }
    });

    window.sig.refresh();
  });

  window.sig.bind('clickStage', function () {
    window.sig.graph.nodes().forEach(function (n) {
      n.color = n.originalColor;
    });

    window.sig.graph.edges().forEach(function (e) {
      e.color = e.originalColor;
    });

    window.sig.refresh();
  });
}

$(document).foundation();
$(document).ready(initializeGraph);
