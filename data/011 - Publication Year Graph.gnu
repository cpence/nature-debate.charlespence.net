set term pdfcairo font "Myriad Pro,9" linewidth 3 rounded fontscale 1.0
set datafile separator ","

set style line 80 lt rgb "#808080"
set border 3 back linestyle 80
set xtics nomirror
set ytics nomirror

set style line 1 lt rgb "#A00000" lw 1 pt 1
set style line 2 lt rgb "#00A000" lw 1 pt 6
set style line 3 lt rgb "#5060D0" lw 1 pt 2
set style line 4 lt rgb "#F25900" lw 1 pt 9

set xlabel "Year"
set ylabel "Articles Published"

set xrange [1872:1940]
set yrange [0:0.05]

set output '011 - Publication Year Graph.pdf'
plot '010 - Publication Year Graph Data.csv' using 1:2 title "" w l ls 1
